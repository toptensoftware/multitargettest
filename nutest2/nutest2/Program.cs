﻿using System;

namespace nutest2
{
    class Program
    {
        static void Main(string[] args)
        {
            var lib = new MainLibrary.Class1();
            Console.WriteLine($"{lib.Framework} {lib.Platform}");
        }
    }
}
