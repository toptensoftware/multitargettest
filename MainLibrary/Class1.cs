﻿using System;
using SkiaSharp;

namespace MainLibrary
{
    public partial class Class1
    {
        public SKBitmap bitmap = new SKBitmap();

#if OSX
        public OsxOnlyLibrary.Class1 osxRef = null;
#endif

        public NetStandardLibrary.Class1 netRef = null;

        public string Framework
        {
#if NET4
            get => "net4";
#endif
#if NETCORE
            get => "netcore";
#endif
        }

        public string Platform
        {
            get => impl_GetPlatform();
        }

    }
}
